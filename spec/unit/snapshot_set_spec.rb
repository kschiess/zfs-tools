require 'spec_helper'

# For date/time manipulation (spec only)
require 'active_support'

require 'snapshot_set'

describe SnapshotSet do
  attr_reader :present
  before(:each) do
    @present = Time.now
  end
  
  describe SnapshotSet::Snapshot do
    describe "<- #to_s" do
      it "should yield #name" do
        SnapshotSet::Snapshot.new('foobar').to_s.should == 'foobar'
        SnapshotSet::Snapshot.new('19870605something').to_s.should == '19870605something'
      end 
    end

    context "YYYY-MM-DD-HHMMSS" do
      attr_reader :snapshot
      before(:each) do
        @snapshot = SnapshotSet::Snapshot.new('2012-02-15-213516')
      end
      
      it "should have timestamp 15.2.2012 21:35:16" do
        snapshot.time.should == Time.mktime(2012,2,15,21,35,16)
      end
    end
    context "YYYYMMDDstring" do
      attr_reader :snapshot
      before(:each) do
        @snapshot = SnapshotSet::Snapshot.new('19870605something')
      end
      
      it "should have name 19870605something" do
        snapshot.name.should == '19870605something'
      end 
      it "should have timestamp 5.6.1987" do
        snapshot.time.should == Time.mktime(1987,6,5)
      end
    end
    context "YYYYMMDDHHMMstring" do
      attr_reader :snapshot
      before(:each) do
        @snapshot = SnapshotSet::Snapshot.new('198706051314something')
      end
      
      it "should have name 19870605something" do
        snapshot.name.should == '198706051314something'
      end 
      it "should have timestamp 5.6.1987 13:14" do
        snapshot.time.should == Time.mktime(1987,6,5, 13, 14)
      end
    end
    context "YYYY-MM-DD_HH-MMstring" do
      attr_reader :snapshot
      before(:each) do
        @snapshot = SnapshotSet::Snapshot.new('1987-06-05_13-14something')
      end
      
      it "should have name 1987-06-05-13_14something" do
        snapshot.name.should == '1987-06-05_13-14something'
      end 
      it "should have timestamp 5.6.1987 13:14" do
        snapshot.time.should == Time.mktime(1987,6,5, 13, 14)
      end
    end
    context "some_string" do
      attr_reader :snapshot
      before(:each) do
        @snapshot = SnapshotSet::Snapshot.new('some_string')
      end
      
      it "should preserve the name to be some_string" do
        snapshot.name.should == 'some_string'
      end
      it "should have a recent timestamp (we don't known, better keep it)" do
        snapshot.time.should > 10.seconds.ago # if this flubbers, buy a new computer
      end
    end
  end

  # Creates a fixture snapshot with some time jitter added. 
  #
  def snapshot(secs_ago, jitter=0.1)
    # A time in the past that is roughly secs_ago seconds ago:
    rand_part = jitter*secs_ago
    time = (secs_ago + rand(2*rand_part) - rand_part).ago(present)
    
    time.strftime('%Y%m%d%H%M')
  end

  describe "<- #gpc" do
    context "without any snapshots" do
      attr_reader :set
      before(:each) do
        @set = SnapshotSet.new([])
      end
      
      it "should return an empty set" do
        set.gpc(1.day => 10).to_a.should be_empty
      end 
    end
    context "when using a simple fixture (no jitter)" do
      attr_reader :set
      before(:each) do
        @set = SnapshotSet.new([
          'old_and_string', 
          snapshot(3.days, 0),
          snapshot(2.days, 0),
          snapshot(1.day, 0),
          'new_and_string'
        ])
      end

      it "should sort snapshots without timestamp last" do
        set.snapshots.last(2).map(&:name).should include('old_and_string')
        set.snapshots.last(2).map(&:name).should include('new_and_string')
      end 
      it "should sort snapshots from oldest to newest" do
        sorted = set.snapshots
        sorted.map(&:name).first(3).should == [
          snapshot(3.days, 0),
          snapshot(2.days, 0),
          snapshot(1.day, 0)
        ]
      end 
    end
    context "when using fixture spaced out over 2 weeks" do
      attr_reader :set
      before(:each) do
        @set = SnapshotSet.new([
          snapshot(2.weeks), 
          snapshot(2.weeks), 
          snapshot(1.week), 
          snapshot(1.week + 1.day), 
          snapshot(1.week + 2.day), 
          snapshot(3.days),
          snapshot(2.days),
          snapshot(2.days),
          snapshot(1.days),
          snapshot(1.days),
          *Array(0..23).map { |i| snapshot(i.hours) }
        ])
      end
      def call(opts)
        set.gpc(opts, now)
      end

      context "hours(3), days(3), weeks(1)" do
        attr_reader :result
        before(:each) do
          @result = set.gpc(
            1.day => 3, 
            1.hour => 3, 
            1.week => 1
          )
        end
        
        it "should have 4 snapshots in > 1.day.ago" do
          result.snapshots.
            select { |s| s.time > 5.hours.ago(present) }.
            should have(4).entries
        end 
        it "should have 3 snapshots in 10.hours.ago .. 4.days.ago" do
          result.snapshots.
            select { |s| 
              s.time < 10.hours.ago(present) && 
              s.time > 4.days.ago(present)
            }.
            should have(3).entries
        end 
      end
    end
    context "when using hand coded fixture (days)" do
      attr_reader :set, :now
      before(:each) do
        @now = Time.mktime(2010, 1, 20, 0, 0)
        @set = SnapshotSet.new([
          '20100120today', 
          '20100119yesterday', 
          '20100118twodaysago', 
          '201001180001twodaysago_dupe', 
          '20100117threedaysago', 
          '20100116fourdaysago', 
        ])
      end
      def call(opts)
        set.gpc(opts, now)
      end

      context "kept snapshots with days(1)" do
        attr_reader :kept
        before(:each) do
          @kept = call(1.day =>1).snapshots.map(&:name)
        end

        it "should keep today and yesterday" do
          kept.should include('20100120today')
          kept.should include('20100119yesterday')
        end
        it "should have 2 entries" do
          kept.should have(2).entries
        end 
      end
      context "kept snapshots with days(3)" do
        attr_reader :kept
        before(:each) do
          @kept = call(1.day => 3).snapshots.map(&:name)
        end

        it "should keep 4 days" do
          kept.should include('20100120today')
          kept.should include('20100119yesterday')
          kept.should include('20100118twodaysago')
          kept.should include('20100117threedaysago')
        end
        it "should have 4 entries" do
          kept.should have(4).entries
        end 
      end
      
      context "days(0) (regression)" do
        it "should return the full list" do
          call(1.day => 0).to_a.should == ['20100120today']
        end 
      end
      
      describe "<- #to_a" do
        it "should yield the original array" do
          set.to_a.should == [
            "20100116fourdaysago", 
            "20100117threedaysago", 
            "20100118twodaysago", 
            "201001180001twodaysago_dupe", 
            "20100119yesterday", 
            "20100120today"]
        end 
      end
    end
  end
end
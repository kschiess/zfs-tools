require 'spec_helper'

require 'zfs'

describe ZFS::SnapshotList, "constructed with short sample input" do
  attr_reader :list
  before(:each) do
    @list = ZFS::SnapshotList.new(<<-ZFSLIST
    pool1
    pool1/active
    pool1/active@201004062100
    pool1/active@201004221100
    pool1/active@201004231100
    pool1/active@201004241100
    pool1/active@201004251100
    pool1/active@201004261100
    pool1/active@201004290900
    pool1/active@201004291000
    pool1/active@201004291100
    pool1/active/belle
    pool1/active/belle@201004081300
    pool1/active/belle@201004221100
    pool1/active/belle@201004231100
    pool1/active/belle@201004241100
    pool1/active/belle@201004281700
    pool1/active/belle@201004281800
    ZFSLIST
    )
  end

  describe "<- #datasets" do
    it "should contain 'pool1'" do
      list.datasets.should include('pool1')
    end
    it "should contain 'pool1/active'" do
      list.datasets.should include('pool1/active')
    end
    it "should contain 'pool1/active/belle'" do
      list.datasets.should include('pool1/active/belle')
    end   
    it "should contain three snapshots" do
      list.datasets.should have(3).snapshots
    end 
  end
  describe "<- #snapshots" do
    context "'pool1'" do
      it "should have no snapshots" do
        list.snapshots('pool1').should be_empty
      end 
    end
    context "'pool1/active'" do
      def call
        list.snapshots('pool1/active')
      end
      it "should have 9 snapshots" do
        call.should have(9).snapshots
      end 
      it "should include '201004062100'" do
        call.should include('201004062100')
      end  
    end
  end
end
require 'spec_helper'

require 'zfs'

describe ZFS::Dataset do
  attr_reader :dataset
  before(:each) do
    @dataset = ZFS::Dataset.new('pool1/backup/active')
    
    # Stub real tool with output from zfs list -rH -oname pool1/backup
    flexmock(dataset, 
      :list => <<-EOS
pool1/backup
pool1/backup/active
pool1/backup/active@201003301822
pool1/backup/active@201003301824
pool1/backup/active/gironimo
pool1/backup/active/gironimo@201003301822
pool1/backup/active/gironimo@201003301824
pool1/backup/active/ls3
pool1/backup/active/ls3@2010-03-03_16-48
pool1/backup/active/ls3@201003301822
pool1/backup/active/ls3@201003301824
pool1/backup/active/ls7
pool1/backup/active/ls7@201003301822
pool1/backup/active/ls7@201003301824
pool1/backup/active/permosdata
pool1/backup/active/permosdata@201003301822
pool1/backup/active/permosdata@201003301824
pool1/backup/active/tiva
pool1/backup/active/tiva@201003301822
pool1/backup/active/tiva@201003301824
pool1/backup/active/toodles
pool1/backup/active/toodles@201003301822
pool1/backup/active/toodles@201003301824
      EOS
      )
  end
  
  describe "<- #snapshots" do
    it "should output a list of snapshots" do
      dataset.snapshots.should == ["201003301822", "201003301824"]
    end 
  end
  describe "<- #snapshot" do
    it "should snapshot with the given name" do
      flexmock(dataset).
        should_receive(:zfs_snapshot).with("'pool1/backup/active@given_name'").once
        
      dataset.snapshot('given_name')
    end 
    it "should transform illegal characters" do
      flexmock(dataset).
        should_receive(:zfs_snapshot).with("'pool1/backup/active@010203-word_______-_'").once
        
      dataset.snapshot('010203-word !!! _ - ')
    end 
    it "should allow recursive snapshots" do
      flexmock(dataset).
        should_receive(:zfs_snapshot).with('-r', "'pool1/backup/active@name'").once
        
      dataset.snapshot('name', true)
    end 
  end
  describe "<- #snapshot_with_timestamp" do
    it "should create a snapshot that includes the current time" do
      time = Time.now
      
      flexmock(dataset).
        should_receive(:snapshot).with(
          time.strftime("%Y%m%d%H%M-comment"), 
          true).once
      
      dataset.snapshot_with_timestamp('comment', time)
    end 
    it "should return the name of the snapshot that was created" do
      flexmock(dataset).
        should_receive(:zfs_snapshot)
        
      dataset.snapshot_with_timestamp('test/test').should match(/^\d{12}-test_test$/)
    end
  end
end

# A small collection of zfs classes that rely on zfs utilities.
#
module ZFS; end

require 'zfs/snapshot_list'
require 'zfs/dataset'



# Represents a list of snapshots like the tool 'zfs list' outputs it. This 
# class allows extracting information from that list. 
#
# Example: 
#
#   snl = ZFS::SnapshotList.new(`zfs list -H -o name`)
#   snl.datasets # => array of dataset names
#   snl.snapshots('pool1') # => array of snapshots on the pool1
#
class ZFS::SnapshotList
  def initialize(tool_output)
    @list = Hash.new { |h,k| h[k] = [] }
    
    tool_output.lines. 
      map { |l| l.chomp.strip.split('@').first(2) }.  # extracts path/snapshot tuples
      each { |path, snapshot|
        snapshots = @list[path]
        snapshots << snapshot if snapshot }
        
  end
  def datasets
    @list.keys
  end
  def snapshots(dataset_name)
    @list[dataset_name]
  end
end
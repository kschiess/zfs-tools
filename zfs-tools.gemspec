# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{zfs-tools}
  s.version = "0.2.1"

  s.authors = ["Kaspar Schiess"]
  s.email = %q{kaspar.schiess@absurd.li}
  s.executables = [
    "zfs_safe_destroy", 
    "zfs_list_obsolete_snapshots", 
    "zfs_snapshot"]
  s.files = %w(LICENSE HISTORY README) + Dir.glob("{lib,bin}/**/*")
  s.homepage = %q{http://bitbucket.org/kschiess/zfs-tools}
  s.require_paths = ["lib"]
  s.summary = %q{
    A few ZFS tools, mostly related to snapshotting, cleaning up and synching. }
  
  s.add_dependency 'mixlib-shellout', '>= 1.0'
  s.add_dependency 'activesupport', '>= 3.1'
  s.add_dependency 'i18n', '>= 0.6'
end
